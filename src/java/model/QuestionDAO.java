/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.List;

public class QuestionDAO extends MyDAO{

    public  List<Question> getAllQuestions() {
        List<Question> questions = new ArrayList<Question>();
        String xSql = "select * from tblQuestion";

        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Question question = new Question();
                question.setId(rs.getInt("id"));
                question.setQuestion(rs.getString("question"));
                question.setA(rs.getString("a"));
                question.setB(rs.getString("b"));
                question.setC(rs.getString("c"));
                question.setD(rs.getString("d"));
                question.setAnwser(rs.getString("answer"));
                questions.add(question);
            }
        rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (questions);
    }
    public static void main(String[] args) throws Exception {
        QuestionDAO dao = new QuestionDAO();
        System.out.println(dao.getAllQuestions().size());
    }
}
